package com.devcamp.jpahyberate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jpahyberate.model.CVoucher;
import com.devcamp.jpahyberate.repository.IVoucherRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers(){
        
        try {
            List<CVoucher> listVoucher = new ArrayList<CVoucher>();
          /*   voucherRepository.findAll().forEach((ele)->{
                listVoucher.add(ele);
            });
          */
            voucherRepository.findAll().forEach(listVoucher::add);
            if (listVoucher.size() == 0){
                return new ResponseEntity<>(listVoucher, HttpStatus.NOT_FOUND);
            }else{
                return new ResponseEntity<>(listVoucher, HttpStatus.OK);
            }
            

        }
        catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/voucher5")
    public ResponseEntity<List<CVoucher>> getFiveVoucher(
                        @RequestParam(value = "page", defaultValue = "1") String page,
                       @RequestParam(value = "size", defaultValue = "5") String size) {
       try {
           PageRequest pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
           List<CVoucher>  list = new ArrayList<CVoucher>();
           voucherRepository.findAll(pageWithFiveElements).forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null;
       }
    } 

}
